

#include <iostream>


using namespace std;
class Player
{
public:
    char name[256] = "name";
    int score = 0;
    void AddPlayer() // Adding the player's name and his points
    {
        cout << "Write the player's name";
        cin >> name;
        cout << "How many points does the player have";
        cin >> score;
    }
    void ShowPlayer() // display the values on the screen
    {
        cout <<"name: " <<  name << "\t" <<  "score: " << score << endl;
    }
};

int main()
{
    int size = 0;
    cout << "enter the number of players";
    cin >> size; // enter the number of players
    Player* Array = new Player[size];
    for (int i = 0; i < size; ++i)
    {
        cout << i + 1 << ' ' << "Player and score: ";
        Array[i].AddPlayer(); // enabling the Add Player() function
    }
    cout << endl;

    
    // Sorting results
    for (int i = 0; i < size; ++i)
    {
        for (int j = size - 1; j > i; j--)
        {
           if ((Array[j].score) > (Array[j - 1].score))
            {
                swap(Array[j], Array[j - 1]);
            }
        }
    }
    cout << endl;
    cout << "sorted tab" << endl;
    for (int i = 0; i < size; ++i)
    {
        Array[i].ShowPlayer(); // output of the sorting result
    }
}



